package config

import (
    "strings"

	"gopkg.in/yaml.v2"
	"encoding/xml"
	"encoding/json"
)

type Parser interface {
	IsApplicable(fileName *string) bool
	Parse(in []byte, out interface{})
}

type YamlParser struct {
}

func (yp *YamlParser) IsApplicable(fileName *string) bool {
	return strings.Contains(*fileName, ".yaml")
}

func (yp *YamlParser) Parse(in []byte, out interface{}) {
	yaml.Unmarshal(in, out)
}

type XmlParser struct {
}

func (xp *XmlParser) IsApplicable(fileName *string) bool {
	return strings.Contains(*fileName, ".xml")
}

func (xp *XmlParser) Parse(in []byte, out interface{}) {
	xml.Unmarshal(in, out)
}

type JsonParser struct {
}

func (jp *JsonParser) IsApplicable(fileName *string) bool {
	return strings.Contains(*fileName, ".json")
}

func (jp *JsonParser) Parse(in []byte, out interface{}) {
	json.Unmarshal(in, out)
}

