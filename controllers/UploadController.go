package controllers

import (
	"strings"
	"net/http"
	"encoding/json"
	"golang.server/logs"
	"golang.server/archives"
	"golang.server/managers"
	os "golang.server/files"
	"golang.server/constants"
)

const MAX_MEMORY_LIMIT = 100000
const REQUEST_FILES_PART = "files"

func UploadFile(r *http.Request) (int, string) {
	var fileNames []string
	err := r.ParseMultipartForm(MAX_MEMORY_LIMIT)

	if err != nil {
	return http.StatusInternalServerError, err.Error()
	}

	files := r.MultipartForm.File[REQUEST_FILES_PART]

	for i, _ := range files {
		dst, err := os.SaveFile(files[i], os.UploadsDir)
		if err != nil {
			return http.StatusInternalServerError, err.Error()
		}

		logs.CurrentLogger.Info("Saved %q", dst)
		fileNames = append(fileNames, dst)
 	}

	managers.FileMg.InsertAll(fileNames)
	return 200, HandleRequest(fileNames)
}

func UploadArchive(r *http.Request) (int, string) {
	err := r.ParseMultipartForm(MAX_MEMORY_LIMIT)
	var fileNames []string

	if err != nil {
		return http.StatusInternalServerError, err.Error()
	}

	files := r.MultipartForm.File[REQUEST_FILES_PART]
	var unarchiver archives.ArchiveWorker

	for i, _ := range files {
		fileName := files[i].Filename

		unarchiver = archives.FindArchiveWorker(fileName)
		if unarchiver == nil {
			continue
		}

		dst, saveError := os.SaveFile(files[i], os.UploadsDir)
		if saveError != nil {
			logs.CurrentLogger.Err("%q", saveError.Error())
			return http.StatusInternalServerError, saveError.Error()
		}

		names, unarchiveError := unarchiver.Unarchive(dst, os.UploadsDir)
		if unarchiveError != nil {
			logs.CurrentLogger.Err("%q", unarchiveError.Error())
			return http.StatusInternalServerError, unarchiveError.Error()
		}

		for _, name := range names {
			fileNames = append(fileNames, name)
		}
	}

	managers.FileMg.InsertAll(fileNames)
	return 200, HandleRequest(fileNames)
}

func HandleRequest(fileNames []string) string {
	var finalNames []string

	for _, fn := range fileNames {
		finalNames = append(finalNames, strings.Replace(fn, os.UploadsDir, constants.BASE_SERVER_PATH, 1))
	}

	json, _ := json.Marshal(finalNames)
	return string(json)
}
