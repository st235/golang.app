package archives

import (
	"strings"
	"path/filepath"
	"os"
	"io"
	"archive/tar"
)

type TarWorker struct {
}

func (tw *TarWorker) IsApplicable(fileName string) bool {
	return strings.Contains(fileName, ".tar")
}

func (tw *TarWorker) Unarchive(src, dest string) ([]string, error) {
	var filenames []string

	reader, err := os.Open(src)
	if err != nil {
		return nil, err
	}
	defer reader.Close()
	tarReader := tar.NewReader(reader)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		path := filepath.Join(dest, header.Name)
		filenames = append(filenames, path)

		info := header.FileInfo()
		if info.IsDir() {
			if err = os.MkdirAll(path, info.Mode()); err != nil {
				return nil, err
			}
			continue
		}

		file, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, info.Mode())
		if err != nil {
			return nil, err
		}
		defer file.Close()
		_, err = io.Copy(file, tarReader)
		if err != nil {
			return nil, err
		}
	}

	return filenames, nil
}