# File Server (Go Lang)

A simple file server for downloading and retrieving files. Can unpack archives. I hope you will be successful in use :stuck_out_tongue_winking_eye:.

## Usage

### Start

To start the application, 
you can use a binary file in the archive or compile it yourself using the command:

```bash
go build
```

The application takes arguments from the command line to view the following command:

```bash
./golang.server -h
```

```bash
Usage of ./golang.server:
  -config string
        pass a path to config
  -log string
        pass enable to turn on logs (default "disabled")
```

Example of start command

```bash
./golang.server -config="/Users/test/Downloads/Telegram Desktop/go/config.yaml" -log=enable
```

### Config file

The server can be configured using xml, yaml or json file.
If you need to provide your type of configuration files, read below.

```yaml
http:
  port: 8080
  timeout: 5000
database:
  connection_uri: "alex:12345678@/"
  database_name: "godb"
uploads:
  uploads_dir: "/Users/alexander/Downloads/Telegram Desktop/uploads/"
```

Let's look at the example provided in details.

#### Http (the part response for http-server configuration)

 - **port** - _optional, by default: 3000, integer._ Represents the deployment port of application.
 - **timeout** - _optional, integer._ Current timeout of connection.

#### Database (part provides database config)

 - **connection_uri** - _required, string._ Represent the connection link to mysql database in the following format scheme:[//[user[:password]@]host[:port]][/path][?query][#fragment]
 - **database_name** - _required, string._ The name of your database. If the database not exists, it will be created by the first server start.

#### Uploads (part provides file storage config)

 - **uploads_dir** - _optional, be default: "./uploads", string._ The folder where uploaded files would be stored. If the folder doesnt exists, it will be created by the first server start.

## Routes

### /upload (POST)

Uploads provided file(-s) to directory on server.

Takes in **body**

files: multipart, maybe an array

Response:

```json
["http://192.168.1.2:8080/sample.yaml"]
```

### /upload/archive (POST)

Uploads provided archive(-s) **(only archives!)** and unpack it to directory on server.

Takes in **body**

files: multipart, maybe an array

Response:

```json
["http://192.168.1.2:8080/photo_2017-10-06_22-00-32.jpg",
"http://192.168.1.2:8080/photo_2017-10-07_22-49-41.jpg",
"http://192.168.1.2:8080/photo_2017-10-07_22-54-05.jpg"]
```

### /get/all/files (GET)

Takes in **queries**

limit: the size of retrieved array

offset: the amount of pages that will be offsets

Response:

```json
[
{
"Id": 1,
"Url": "http://192.168.1.2:8080/sample.yaml",
"FileName": "sample.yaml",
"CreatedDate": "2017-10-14 15:39:38"
}
]
```

## Create your own config format

Create a read of your own config format is pretty simple.

_First_, you need to define your structure with the interface described.

```go
type Parser interface {
	IsApplicable(fileName *string) bool
	Parse(in []byte, out interface{})
}
```

_Second_, you need to add your structure instance to list of parsers in **config/Utils.go**

```go
var parsers []Parser = []Parser{new(YamlParser), new(XmlParser), new(JsonParser)}
```

## Create your own unarchiver

To add your own archive worker you need follow the instruction:

_First_, you need to define your structure with the interface described.

```go
type ArchiveWorker interface {
	IsApplicable(fileName string) bool
	Unarchive(src, dest string) ([]string, error)
}
```

_Second_, you need to add your structure instance to list of parsers in **archives/Utils.go**

```go
var workers []ArchiveWorker = []ArchiveWorker{new(ZipWorker)}
```

## License

MIT License

Copyright (c) 2017 Alexander Dadukin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
