package models

type FileModel struct {
	Id int
	Url string
	FileName string
	CreatedDate string
}