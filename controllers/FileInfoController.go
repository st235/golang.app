package controllers

import (
	"net/http"
	"encoding/json"
	"golang.server/managers"
	"strconv"
)

func GetFileInfo(r *http.Request) (int, string) {
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		limit = 20
	}

	offset, err := strconv.Atoi(r.URL.Query().Get("offset"))
	if err != nil {
		offset = 0
	}

	offset *= limit

	rslt := managers.FileMg.ObtainAll(limit, offset)
	json, _ := json.Marshal(rslt)
	return 200, string(json)
}
