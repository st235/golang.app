package files

import (
	"os"
	"golang.server/logs"
)

const UPLOADS_DIR = "./uploads/"

var UploadsDir string

func CheckUploadsDirectory() {
	if _, err := os.Stat(UploadsDir); os.IsNotExist(err) {
		logs.CurrentLogger.Warn("Creating current uploads directory, because its does not exists")
		os.Mkdir(UploadsDir, os.ModePerm)
	} else {
		logs.CurrentLogger.Info("Uploads files exists " + UploadsDir)
	}
}
