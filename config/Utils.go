package config

import (
	"golang.server/logs"
	"reflect"
)

var reader Reader = new(ConfigReader)
var parsers []Parser = []Parser{new(YamlParser), new(XmlParser), new(JsonParser)}

func SetReader(rdr Reader) {
	reader = rdr
}

func Unmarshal(configPath *string, out interface{}) {
	data := reader.Scan(configPath)

	for i := 0; i < len(parsers); i++  {
		if !parsers[i].IsApplicable(configPath) {
			continue
		}

		logs.CurrentLogger.Warn("Selected logger: %s", reflect.TypeOf(parsers[i]))
		parsers[i].Parse(data, out)
		break
	}
}