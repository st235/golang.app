package logs

import (
	color "github.com/fatih/color"
)

type Logger interface {
	Info(message string, a... interface{})
	Warn(message string, a... interface{})
	Err(message string, a... interface{})
}

type ProductionLogger struct {
}

func (pl *ProductionLogger) Info(message string, a... interface{}) {
}

func (pl *ProductionLogger) Warn(message string, a... interface{}) {
}

func (pl *ProductionLogger) Err(message string, a... interface{}) {
}

type DebugLogger struct {
}

func (dl *DebugLogger) Info(message string, a... interface{}) {
	if len(a) == 0 {
		color.Cyan(message)
		return
	}
	color.Cyan(message, a)
}

func (dl *DebugLogger) Warn(message string, a... interface{}) {
	if len(a) == 0 {
		color.Yellow(message)
		return
	}
	color.Yellow(message, a)
}

func (dl *DebugLogger) Err(message string, a... interface{}) {
	if len(a) == 0 {
		color.Red(message)
		return
	}
	color.Red(message, a)
}