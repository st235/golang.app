package router

import (
	"github.com/go-martini/martini"
	"database/sql"
	"golang.server/controllers"
	"golang.server/logs"
	"os"
	"strconv"
	"golang.server/files"
)

const PORT_ENV = "PORT"

type Router struct {
	ServerApp *martini.ClassicMartini
	DatabaseApp *sql.DB
}

func (r *Router) Route(port int) {
	logs.CurrentLogger.Info("Set port to %d", port)
	os.Setenv(PORT_ENV, strconv.Itoa(port))

	r.ServerApp.Post("/upload", controllers.UploadFile)
	r.ServerApp.Post("/upload/archive", controllers.UploadArchive)

	r.ServerApp.Get("/get/all/files", controllers.GetFileInfo)

	r.ServerApp.Use(martini.Static(files.UploadsDir))
}
