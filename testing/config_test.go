package testing

import (
	"testing"
	"golang.server/config"
	"golang.server/models"
	"strings"
	"golang.server/logs"
)

type TestConfigReader struct {
	file string
}

func (cr *TestConfigReader) Scan(path *string) []byte {
	logs.CurrentLogger.Info(cr.file)
	return []byte(cr.file)
}

func TestParsingWhenJsonFileNormal(t *testing.T) {
	tr := TestConfigReader{}
	config.SetReader(&tr)

	tr.file = `
	{
		"http": {
			"port": 2000,
			"timeout": 1500
		},
		"database": {
			"connection_uri": "http://lc:12312"
		}
	}
	`

	fileName := "test.json"
	configModel := new(models.ConfigModel)
	config.Unmarshal(&fileName, configModel)

	if configModel.Http.Port != 2000 {
		t.Error("Expected 2000 got ", configModel.Http.Port)
	}

	if configModel.Http.Timeout != 1500 {
		t.Error("Expected 1500 got ", configModel.Http.Timeout)
	}

	if strings.Compare(configModel.Database.ConnectionUri, "http://lc:12312") != 0 {
		t.Error("Expected http://lc:12312 got ", configModel.Database.ConnectionUri)
	}
}

func TestParsingWhenJsonBeaten(t *testing.T) {
	tr := TestConfigReader{}
	config.SetReader(&tr)

	tr.file = `
	{
		"http": {
			"port": 2000,
		}
	}
	`

	fileName := "test.json"
	configModel := new(models.ConfigModel)
	config.Unmarshal(&fileName, configModel)

	if configModel.IsValid() {
		t.Error("Expected invalid model but got valid")
	}
}

func TestParsingWhenYamlFileNormal(t *testing.T) {
	tr := TestConfigReader{}
	config.SetReader(&tr)

	tr.file = `
http:
  port: 3000
  timeout: 5000
database:
  connection_uri: "http://localhost:127012"
`

	fileName := "test.yaml"
	configModel := new(models.ConfigModel)
	config.Unmarshal(&fileName, configModel)

	if configModel.Http.Port != 3000 {
		t.Error("Expected 3000 got ", configModel.Http.Port)
	}

	if configModel.Http.Timeout != 5000 {
		t.Error("Expected 5000 got ", configModel.Http.Timeout)
	}

	if strings.Compare(configModel.Database.ConnectionUri, "http://localhost:127012") != 0 {
		t.Error("Expected http://localhost:127012 got ", configModel.Database.ConnectionUri)
	}
}

func TestParsingWhenYamlFileBeaten(t *testing.T) {
	tr := TestConfigReader{}
	config.SetReader(&tr)

	tr.file = `
http:
  port: "hello world"
  timeout: 5000
database:
  connection_uri: "http://localhost:127012"
`

	fileName := "test.yaml"
	configModel := new(models.ConfigModel)
	config.Unmarshal(&fileName, configModel)

	if configModel.IsValid() {
		t.Error("Expected invalid model but got valid")
	}
}

func TestParsingWhenXmlFileNormal(t *testing.T) {
	tr := TestConfigReader{}
	config.SetReader(&tr)

	tr.file = `
<config>
    <http>
        <port>3000</port>
        <timeout>5000</timeout>
    </http>
    <database>
        <connection_uri>http://localhost:127012</connection_uri>
    </database>
<config>
`

	fileName := "test.xml"
	configModel := new(models.ConfigModel)
	config.Unmarshal(&fileName, configModel)

	if configModel.Http.Port != 3000 {
		t.Error("Expected 3000 got ", configModel.Http.Port)
	}

	if configModel.Http.Timeout != 5000 {
		t.Error("Expected 5000 got ", configModel.Http.Timeout)
	}

	if strings.Compare(configModel.Database.ConnectionUri, "http://localhost:127012") != 0 {
		t.Error("Expected http://localhost:127012 got ", configModel.Database.ConnectionUri)
	}
}

func TestParsingWhenXmlFileBeaten(t *testing.T) {
	tr := TestConfigReader{}
	config.SetReader(&tr)

	tr.file = `
<config></config>
`

	fileName := "test.xml"
	configModel := new(models.ConfigModel)
	config.Unmarshal(&fileName, configModel)

	if configModel.IsValid() {
		t.Error("Expected invalid model but got valid")
	}
}