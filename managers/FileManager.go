package managers

import (
	"fmt"
	"database/sql"
	"golang.server/logs"
	"golang.server/models"
	"golang.server/database"
	"strings"
	os "golang.server/files"
	"golang.server/constants"
)

type FileManager struct {
	DB *sql.DB
}

func (fm *FileManager) InsertAll(files []string) {
	query := fmt.Sprintf("INSERT INTO %s (file_name) VALUES( ? )", database.FILE_TABLE);
	stmtIns, err := fm.DB.Prepare(query)
	if err != nil {
		panic(err.Error())
	}
	defer stmtIns.Close()

	for _, file := range files {
		fileName := strings.Replace(file, os.UploadsDir, "", 1)
		_, err = stmtIns.Exec(fileName)
		if err != nil {
			logs.CurrentLogger.Err("There was an error while trying to prepare: %s", query)
		}
	}
}

func (fm *FileManager) Obtain(fileName string) *models.FileModel {
	query := fmt.Sprintf("SELECT file_name AS 'name' FROM %s WHERE file_name = ?", database.FILE_TABLE)
	logs.CurrentLogger.Info(query)

	stmtOut, err := fm.DB.Prepare(query)
	if err != nil {
		logs.CurrentLogger.Err("There was an error while trying to prepare: %s", query)
	}

	file := new(models.FileModel)

	err = stmtOut.QueryRow(fileName).Scan(&file.FileName)

	defer stmtOut.Close()
	return file
}

func (fm *FileManager) ObtainAll(limit int, offset int) []models.FileModel {
	query := fmt.Sprintf("SELECT id, file_name, created_date FROM %s LIMIT %d OFFSET %d",
		database.FILE_TABLE, limit, offset)
	logs.CurrentLogger.Info(query)

	rows, err := fm.DB.Query(query)
	if err != nil {
		logs.CurrentLogger.Err("There was an error while trying to exec: %s", query)
	}

	var files []models.FileModel

	for rows.Next() {
		var file models.FileModel

		var id int
		var fileName string
		var createdDate string
		err = rows.Scan(&id, &fileName, &createdDate)

		file.Id = id
		file.FileName = fileName
		file.CreatedDate = createdDate

		file.Url = constants.BASE_SERVER_PATH + fileName

		logs.CurrentLogger.Info("%d %q", id, fileName)
		files = append(files, file)
	}

	return files
}
