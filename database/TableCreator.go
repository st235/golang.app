package database

import (
	"bytes"
	"golang.server/logs"
	"golang.server/constants"
)

type TableCreator struct {
	buffer bytes.Buffer
}

type ColumnContentDescriber struct {
	ColumnName string
	ColumnType string
	Options string
}

func (di *TableCreator) CreateTable(tableName string) *TableCreator {
	di.buffer.WriteString("CREATE TABLE IF NOT EXISTS ")
	di.buffer.WriteString(tableName)
	di.buffer.WriteString(" (")
	return di
}

func (di *TableCreator) PrimaryKey(columnName string) *TableCreator {
	di.buffer.WriteString(columnName)
	di.buffer.WriteString(" INT NOT NULL PRIMARY KEY AUTO_INCREMENT, ")
	return di
}

func (di *TableCreator) DescribeColumn(describers ...ColumnContentDescriber) *TableCreator {
	for i, describer := range describers {
		di.buffer.WriteString(describer.ColumnName)
		di.buffer.WriteString(constants.SPACE)
		di.buffer.WriteString(describer.ColumnType)
		di.buffer.WriteString(constants.SPACE)
		di.buffer.WriteString(describer.Options)

		if i == len(describers) - 1 {
			break
		}

		di.buffer.WriteString(constants.COMA)
	}

	return di
}

func (di *TableCreator) Exec() string {
	di.buffer.WriteString(");")

	query := di.buffer.String()
	logs.CurrentLogger.Info(query)
	return query
}

