package args

import (
	"flag"
	"strings"
)

type ArgsProvider struct {
	IsLogEnabled bool
	ConfigPath   *string
}

func (ap *ArgsProvider) Parse() {
	logState := flag.String("log", "disabled", "pass enable to turn on logs")
	ap.ConfigPath = flag.String("config", "", "pass a path to config")
	flag.Parse()

	ap.IsLogEnabled = strings.Compare(*logState, "enable") == 0
}