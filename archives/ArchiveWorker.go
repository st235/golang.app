package archives

type ArchiveWorker interface {
	IsApplicable(fileName string) bool
	Unarchive(src, dest string) ([]string, error)
}
