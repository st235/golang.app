package config

import (
	fio "io/ioutil"
	"golang.server/logs"
)

type Reader interface {
	Scan(path *string) []byte
}

type ConfigReader struct {
}

func (cr *ConfigReader) Scan(path *string) []byte {
	data, err := fio.ReadFile(*path);

	if err != nil {
		logs.CurrentLogger.Err("There was an error while read file. Try another file path")
		panic(err)
	}

	logs.CurrentLogger.Info("File read:\n%s", string(data[:]))
	return data
}