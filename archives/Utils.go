package archives

var workers []ArchiveWorker = []ArchiveWorker{new(ZipWorker), new(TarWorker)}

func FindArchiveWorker(name string) ArchiveWorker {
	for _, worker := range workers {
		if worker.IsApplicable(name) {
			return worker
		}
	}

	return nil
}