package files

import (
	"mime/multipart"
	"os"
	"io"
	"golang.server/logs"
)

func SaveFile(currentFile *multipart.FileHeader, uploadsDir string) (string, error) {
	logs.CurrentLogger.Info("getting handle to file")
	file, err := currentFile.Open()
	defer file.Close()

	if err != nil {
		return "", err
	}

	logs.CurrentLogger.Info("creating destination file")
	dst, err := os.Create(uploadsDir + currentFile.Filename)
	defer dst.Close()

	if err != nil {
		return "", err
	}

	logs.CurrentLogger.Info("copying the uploaded file to the destination file")

	if _, err := io.Copy(dst, file); err != nil {
		return "", err
	}

	return dst.Name(), nil
}
