package constants

import "fmt"

const COMA = ", "
const SPACE = " "
const NOT_NULL  = " NOT NULL "

const INT = " INT "
const TEXT = " TEXT "
const TIME = " TIMESTAMP "

func VARCHAR(mem rune) string {
	return fmt.Sprintf(" VARCHAR(%d) ", mem)
}
