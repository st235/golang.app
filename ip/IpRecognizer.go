package ip

import (
	"net"
	"os"
	"golang.server/logs"
	"fmt"
)

func GetMachineIp(port int) string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		logs.CurrentLogger.Err("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				logs.CurrentLogger.Info(ipnet.IP.String() + "\n")
				return fmt.Sprintf("http://%s:%d/", ipnet.IP.String(), port)
			}
		}
	}

	return fmt.Sprintf("http://localhost:%d/", port)
}
