package main

import (
	"golang.server/args"
	"golang.server/logs"
	"golang.server/config"
	"golang.server/models"
	"golang.server/database"
	"github.com/go-martini/martini"
	"golang.server/router"
	"golang.server/files"
	"golang.server/managers"
	"golang.server/constants"
	"golang.server/ip"
)

func main() {
	argsProvider := new(args.ArgsProvider)
	argsProvider.Parse()

	logs.InitLogger(argsProvider.IsLogEnabled)
	logs.CurrentLogger.Info("App is started")
	logs.CurrentLogger.Info("Current args: %+v", *argsProvider)

	configModel := new(models.ConfigModel)
	config.Unmarshal(argsProvider.ConfigPath, configModel)

	if !configModel.IsValid() {
		logs.CurrentLogger.Err("The config have an invalid structure")
	} else {
		logs.CurrentLogger.Info("Parsed: %v", *configModel)
	}

	constants.BASE_SERVER_PATH = ip.GetMachineIp(configModel.Http.Port)

	files.UploadsDir = configModel.Uploads.Dir
	files.CheckUploadsDirectory()

	databaseApp := database.Connect(configModel.Database.ConnectionUri)
	database.InitDatabase(databaseApp, configModel.Database.DatabaseName)
	managers.Init(databaseApp)
	defer databaseApp.Close()

	serverApp := martini.Classic()

	router := new(router.Router)
	router.DatabaseApp = databaseApp
	router.ServerApp = serverApp
	router.Route(configModel.Http.Port)
	defer serverApp.Run()
}
