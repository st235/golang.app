package logs

var CurrentLogger Logger = new(ProductionLogger)

func InitLogger(isEnabled bool) {
	if isEnabled {
		CurrentLogger = new(DebugLogger)
		return
	}

	CurrentLogger = new(ProductionLogger)
}
