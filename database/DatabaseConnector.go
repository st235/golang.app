package database

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"golang.server/logs"
	"golang.server/constants"
)

const FILE_TABLE = "files"
const DEFAULT_PRIMARY_KEY = "id"

func Connect(connectionUri string) *sql.DB {
	db, err := sql.Open("mysql", connectionUri)

	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		logs.CurrentLogger.Err("The connection was not open")
		panic(err.Error())
	} else {
		logs.CurrentLogger.Info("Connection opened successfully")
	}

	return db
}

func InitDatabase(db *sql.DB, dbName string) {
	_, crErr := db.Exec("CREATE DATABASE IF NOT EXISTS " + dbName)
	if crErr != nil {
		logs.CurrentLogger.Err("Database cannot be created")
	}

	_, uErr := db.Exec("USE " + dbName)
	if uErr != nil {
		logs.CurrentLogger.Err("Database cannot be selected")
	}

	tc := new(TableCreator)

	ct := tc.
		CreateTable(FILE_TABLE).
		PrimaryKey(DEFAULT_PRIMARY_KEY).
		DescribeColumn(
			ColumnContentDescriber{ ColumnName: "file_name", ColumnType: constants.TEXT, Options: constants.NOT_NULL },
			ColumnContentDescriber{ ColumnName: "created_date", ColumnType: constants.TIME, Options: " DEFAULT CURRENT_TIMESTAMP "}).
		Exec()

	_, tErr := db.Exec(ct)
	if tErr != nil {
		logs.CurrentLogger.Err("Current tables cannot be created")
	}
}
