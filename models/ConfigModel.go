package models

import "golang.server/files"

type ConfigModel struct {
	Http struct {
		Port int  `yaml:"port" json:"port" xml:"port"`
		Timeout int  `yaml:"timeout" json:"timeout" xml:"timeout"`
	} `yaml:"http" json:"http" xml:"http"`

	Database struct {
		ConnectionUri string `yaml:"connection_uri" json:"connection_uri" xml:"connection_uri"`
		DatabaseName string `yaml:"database_name" json:"database_name" xml:"database_name"`
	} `yaml:"database" json:"database" xml:"database"`

	Uploads struct {
		Dir string `yaml:"uploads_dir" json:"uploads_dir" xml:"uploads_dir"`
	} `yaml:"uploads" json:"uploads" xml:"uploads"`
}

func (cm *ConfigModel) IsValid() bool {
	if cm == nil {
		return false
	}

	if cm.Http.Port == 0 {
		cm.Http.Port = 3000
	}

	if cm.Http.Timeout == 0 {
		return false
	}

	if len(cm.Database.ConnectionUri) == 0 {
		return false
	}

	if len(cm.Database.DatabaseName) == 0 {
		return false
	}

	if len(cm.Uploads.Dir) == 0 {
		cm.Uploads.Dir = files.UPLOADS_DIR
	}

	return true
}